package rest

import (
	"github.com/labstack/echo"
	"gitlab.com/attilagombos/project-skeleton-go/app/service"
	"net/http"
)

type Controller struct {
	echoEngine *echo.Echo
	service    *service.Service
}

func NewController(
	echoEngine *echo.Echo,
	service *service.Service,
) *Controller {
	return &Controller{
		echoEngine: echoEngine,
		service:    service,
	}
}

func (c *Controller) start() {
	c.echoEngine.Add(http.MethodGet, "/healthcheck", func(eCtx echo.Context) error {
		return eCtx.JSON(http.StatusOK, "ok")
	})

	apiRoutes := c.echoEngine.Group("/api/v1")
	apiRoutes.Add(http.MethodGet, "/dummy", func(eCtx echo.Context) error {
		resp, err := c.service.Dummy(eCtx.Request().Context())
		if err != nil {
			return err
		}

		return eCtx.JSON(http.StatusOK, resp)
	})
	apiRoutes.Add(http.MethodGet, "/merge-requests", func(eCtx echo.Context) error {
		mergeRequestIds, err := c.service.GetMergeRequestIds()

		if err != nil {
			return err
		}
		return eCtx.JSON(http.StatusOK, mergeRequestIds)
	})
}
