package config

// AppName of the application
const AppName = "project-skeleton-go"

// AppVersion Version of the application
var AppVersion string

type Config struct {
	Port int `mapstructure:"port" default:"80"`

	RedisHost     string `mapstructure:"redis_host"`
	RedisPort     int    `mapstructure:"redis_port"`
	RedisDatabase int    `mapstructure:"redis_database"`

	ProjectName   string
	PollPeriod    int
}

func DefaultConfig() *Config {
	return &Config{
		Port:          80,
		RedisPort:     6379,
		RedisDatabase: 0,
	}
}
