package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/attilagombos/project-skeleton-go/app/schema"
	"io/ioutil"
	"net/http"
	"strconv"
)

type ApiClient struct {
	client *http.Client
}

func NewApiClient(
	client *http.Client,
) *ApiClient {
	return &ApiClient{
		client: client,
	}
}

const projectsUrl = "https://gitlab.com/api/v4/projects/"

func (a* ApiClient) GetProjects (name string) ([]schema.Project, error) {
	req, err := http.NewRequest("GET", projectsUrl, nil)

	if err != nil {
		return nil, err
	}

	q := req.URL.Query()
	q.Add("search", name)
	req.URL.RawQuery = q.Encode()

	resp, err := a.client.Do(req)

	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)

	var projects []schema.Project

	err = json.Unmarshal(data, &projects)

	return projects, nil
}

func (a* ApiClient) GetMergeRequests (projectId int64) ([]schema.MergeRequest, error) {
	req, err := http.NewRequest("GET", projectsUrl + strconv.FormatInt(projectId, 10) + "/merge_requests", nil)

	if err != nil {
		return nil, err
	}

	q := req.URL.Query()
	q.Add("scope", "all")
	req.URL.RawQuery = q.Encode()

	resp, err := a.client.Do(req)

	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)

	var mergeRequests []schema.MergeRequest

	err = json.Unmarshal(data, &mergeRequests)

	return mergeRequests, nil
}

func (a* ApiClient) UpdateMergeRequest(mergeRequest schema.MergeRequest) error {
	body, err := json.Marshal(mergeRequest)

	if err != nil {
		return err
	}

	req, err := http.NewRequest("PUT", projectsUrl +
		strconv.FormatInt(mergeRequest.ProjectId, 10) +
		"/merge_requests/" +
		strconv.FormatInt(mergeRequest.Iid, 10), bytes.NewBuffer(body))

	if err != nil {
		return err
	}

	q := req.URL.Query()
	q.Add("private_token", "Jusu2sk41JyrohtXK_yB")
	req.URL.RawQuery = q.Encode()
	req.Header.Set("Content-Type", "application/json; charset=utf-8")

	resp, err := a.client.Do(req)

	if err != nil {
		return err
	}

	fmt.Println("Set squash to TRUE for merge request with ID " + strconv.FormatInt(mergeRequest.Id, 10))

	defer resp.Body.Close()

	return nil
}
