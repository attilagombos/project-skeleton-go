package storage

import (
	"context"
	"fmt"
	"github.com/gomodule/redigo/redis"
	jsoniter "github.com/json-iterator/go"
	"gitlab.com/attilagombos/project-skeleton-go/app/schema"
	"gitlab.com/attilagombos/project-skeleton-go/log"
	"strconv"
)

type Redis struct {
	redisPool *redis.Pool
	json      jsoniter.API //Use this to be able to save objects as value and use redis tags instead of json ones
}

func NewRedis(redisPool *redis.Pool, json jsoniter.API) *Redis {
	return &Redis{
		redisPool: redisPool,
		json:      json,
	}
}

func (rc *Redis) Close() error {
	return rc.redisPool.Close()
}

func (rc *Redis) closeConn(ctx context.Context, conn redis.Conn) {
	err := conn.Close()
	if err != nil {
		err = schema.SemanticError{Msg: "failed closing redis connection, this might result in memory leak", Err: err}.E()
		log.Warn(ctx, err.Error(), "error", err)
	}
}

func (rc *Redis) GetDummy(ctx context.Context) (*schema.Dummy, error) {
	conn := rc.redisPool.Get()
	defer rc.closeConn(ctx, conn)

	raw, err := conn.Do("GET", "dummy-key")
	if err != nil {
		return nil, redisError{Err: err}.E()
	}
	if raw == nil {
		return nil, nil
	}

	b, ok := raw.([]byte)
	if !ok {
		return nil, redisError{Msg: fmt.Sprintf("invalid type found in redis, expected: []byte, found: %T", raw)}.E()
	}

	dummy := &schema.Dummy{}
	err = rc.json.Unmarshal(b, dummy)
	if err != nil {
		return nil, redisError{Err: err}.E()
	}

	return dummy, nil
}

func (rc *Redis) LoadMergeRequestIds() ([]int64, error) {
	conn := rc.redisPool.Get()
	//defer rc.closeConn(ctx, conn)

	ids, err := redis.Strings(conn.Do("SMEMBERS", "merge-request-ids"))
	if err != nil {
		return nil, redisError{Err: err}.E()
	}
	if ids == nil {
		return nil, nil
	}

	mergeRequestIds := []int64{}

	for _, id := range ids {
		mergeRequestId, err := strconv.ParseInt(id, 10, 64)

		if err == nil {
			mergeRequestIds = append(mergeRequestIds, mergeRequestId)
		}
	}

	return mergeRequestIds, nil
}

func (rc *Redis) StoreMergeRequestId(id int64) error {
	conn := rc.redisPool.Get()
	//defer rc.closeConn(ctx, conn)

	_, err := conn.Do("SADD", "merge-request-ids", id)

	if err != nil {
		return redisError{Err: err}.E()
	}

	return nil
}
