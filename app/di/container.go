package di

import (
	"context"
	"fmt"
	"net/http"
	"reflect"
	"strings"
	"time"

	"github.com/go-playground/validator"
	"github.com/gomodule/redigo/redis"
	jsoniter "github.com/json-iterator/go"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/pkg/errors"
	"gitlab.com/attilagombos/project-skeleton-go/app/api"
	"gitlab.com/attilagombos/project-skeleton-go/app/config"
	"gitlab.com/attilagombos/project-skeleton-go/app/rest"
	"gitlab.com/attilagombos/project-skeleton-go/app/service"
	"gitlab.com/attilagombos/project-skeleton-go/app/storage"
	"gitlab.com/attilagombos/project-skeleton-go/app/validationerr"
	"gitlab.com/attilagombos/project-skeleton-go/log"
)

type Container struct {
	RestServer *rest.Server
	redis      *storage.Redis
	PollService *service.PollService
}

type EchoValidator struct {
	validator *validator.Validate
}

func (cv *EchoValidator) Validate(i interface{}) error {
	err := cv.validator.Struct(i)
	if err != nil {
		return validationerr.ValidationError{Err: err}.E()
	}

	return nil
}

func NewContainer(cfg *config.Config) (*Container, error) {
	c := &Container{}

	c.redis = newRedis(cfg)

	validate := newValidator()

	echoEngine := newEcho(validate)

	apiClient := api.NewApiClient(&http.Client{})

	svc := service.NewService(c.redis, apiClient)

	c.PollService = service.NewPollService(cfg, svc)

	c.RestServer = rest.NewServer(
		cfg.Port,
		echoEngine,
		rest.NewController(
			echoEngine,
			svc,
		),
	)

	return c, nil
}

func newRedis(cfg *config.Config) *storage.Redis {
	redisPool := newRedisPool(cfg)

	redisJSON := jsoniter.Config{
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		OnlyTaggedField:        true,
		TagKey:                 "redis",
	}.Froze()

	return storage.NewRedis(redisPool, redisJSON)
}

func newRedisPool(cfg *config.Config) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     10,
		MaxActive:   10,
		Wait:        true,
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", fmt.Sprintf("%v:%v", cfg.RedisHost, cfg.RedisPort), redis.DialDatabase(cfg.RedisDatabase))
		},
	}
}

func newValidator() *validator.Validate {
	v := validator.New()

	v.RegisterTagNameFunc(func(field reflect.StructField) string {
		name := strings.SplitN(field.Tag.Get("json"), ",", 2)[0]

		if name == "-" {
			name = ""
		}

		return name
	})

	return v
}

func newEcho(validate *validator.Validate) *echo.Echo {
	e := echo.New()

	e.Use(middleware.Recover())
	e.HTTPErrorHandler = rest.DLiveRHTTPErrorHandler
	e.Validator = &EchoValidator{validator: validate}

	return e
}

func (c *Container) Close() {
	err := c.redis.Close()
	if err != nil {
		err = errors.Wrap(err, "redis graceful close failed")
		log.Warn(context.Background(), err.Error(), "error", err)
	}
}
