package service

import (
	"context"
	"fmt"
	"gitlab.com/attilagombos/project-skeleton-go/app/api"
	"gitlab.com/attilagombos/project-skeleton-go/app/schema"
	"gitlab.com/attilagombos/project-skeleton-go/app/storage"
	"strconv"
	"strings"
)

type Service struct {
	redis *storage.Redis
	client *api.ApiClient
	project schema.Project
}

func NewService(redis *storage.Redis, client *api.ApiClient) *Service {
	return &Service{
		redis: redis,
		client: client,
	}
}

func (s *Service) Dummy(ctx context.Context) (*schema.Dummy, error) {
	dummy, err := s.redis.GetDummy(ctx)
	if err != nil {
		return nil, err
	}

	return dummy, nil
}

func (s *Service) InitializeProject(pathWithNamespace string) error {
	projectName := strings.Split(pathWithNamespace, "/")[1]
	projects, err := s.client.GetProjects(projectName)

	if err != nil {
		return err
	}

	for _, project := range projects {
		if project.PathWithNamespace == pathWithNamespace {
			s.project = project
			fmt.Println("Initialized project " + projectName + " with ID " + strconv.FormatInt(project.Id, 10))
			break
		}
	}

	return nil
}

func (s *Service) GetMergeRequestIds() ([]int64, error) {
	mergeRequestsIds, err := s.redis.LoadMergeRequestIds()

	if err != nil {
		return nil, err
	}

	fmt.Println("Retrieved " + strconv.Itoa(len(mergeRequestsIds)) + " merge request IDs")

	return mergeRequestsIds, nil
}

func (s *Service) GetMergeRequests() ([]schema.MergeRequest, error) {
	mergeRequests, err := s.client.GetMergeRequests(s.project.Id)

	if err != nil {
		return nil, err
	}

	fmt.Println("Got " + strconv.Itoa(len(mergeRequests)) + " merge requests for project with ID " +
		strconv.FormatInt(s.project.Id, 10))

	return mergeRequests, nil
}

func (s *Service) UpdateMergeRequests(mergeRequests []schema.MergeRequest) error {
	mergeRequestsIds, err := s.GetMergeRequestIds()

	if err != nil {
		return err
	}

	for _, mergeRequest := range mergeRequests {
		if !contains(mergeRequestsIds, mergeRequest.Id) {
			fmt.Println("Storing new merge request ID " + strconv.FormatInt(mergeRequest.Id, 10))
			s.redis.StoreMergeRequestId(mergeRequest.Id)

			if !mergeRequest.Squash {
				mergeRequest.Squash = true
				s.client.UpdateMergeRequest(mergeRequest)
			}
		}
	}

	return nil
}

func contains(ids []int64, id int64) bool {
	for _, n := range ids {
		if id == n {
			return true
		}
	}
	return false
}
