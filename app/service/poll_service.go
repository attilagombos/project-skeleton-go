package service

import (
	"fmt"
	"gitlab.com/attilagombos/project-skeleton-go/app/config"
	"strconv"
	"time"
)

type PollService struct {
	cfg *config.Config
	service *Service
}

func NewPollService(cfg *config.Config, service *Service) *PollService {
	return &PollService{
		cfg: cfg,
		service: service,
	}
}

func (p *PollService) StartPolling() error {
	err := p.service.InitializeProject(p.cfg.ProjectName)

	if err != nil {
		return err
	}

	fmt.Println("Starting polling project " + p.cfg.ProjectName + " with " +
		strconv.Itoa(p.cfg.PollPeriod) + " seconds period")

	tick := time.NewTicker(time.Duration(p.cfg.PollPeriod) * time.Second)

	p.scheduler(tick)

	return nil
}

func (p *PollService) scheduler(tick *time.Ticker) {
	p.task(time.Now())
	for t := range tick.C {
		p.task(t)
	}
}

func (p *PollService) task(t time.Time) {
	mergeRequests, err := p.service.GetMergeRequests()

	if err == nil {
		p.service.UpdateMergeRequests(mergeRequests)
	}
}
