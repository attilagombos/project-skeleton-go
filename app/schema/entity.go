package schema

type Dummy struct {
	Dummy string `json:"dummy" redis:"dummy"`
}

type Project struct {
	Id int64 `json:"id"`
	PathWithNamespace string `json:"path_with_namespace"`
}

type MergeRequest struct {
	Id int64 `json:"id"`
	Iid int64 `json:"iid"`
	ProjectId int64 `json:"project_id"`
	Squash bool `json:"squash"`
}
