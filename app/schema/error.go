package schema

import (
	"github.com/pkg/errors"
	"gitlab.com/attilagombos/project-skeleton-go/errorsf"
)

const (
	ErrCode     = "code"
	ErrDetails  = "details"
	ErrHTTPCode = "http_code"

	// 400
	ErrValidation = "ERR_VALIDATION"

	// 404
	ErrRouteNotFound = "ERR_ROUTE_NOT_FOUND"

	// 405
	ErrMethodNotAllowed = "ERR_METHOD_NOT_ALLOWED"

	// 500
	ErrRedis         = "ERR_REDIS"
	ErrSemanticError = "ERR_SEMANTIC"
)

type HTTPError struct {
	Error Error `json:"error"`
}

type Error struct {
	Message string                   `json:"message" validate:"required"`
	Code    string                   `json:"code" validate:"required"`
	Details []map[string]interface{} `json:"details,omitempty" validate:"omitempty"`
}

func ToHTTPError(err error) (*HTTPError, int) {
	httpCode := ErrorHTTPCode(err)
	if httpCode == 0 {
		httpCode = 500
	}

	return &HTTPError{
		Error: Error{
			Message: err.Error(),
			Code:    ErrorCode(err),
			Details: ErrorDetails(err),
		},
	}, httpCode
}

func ErrorCode(err error) string {
	field := errorsf.Field(err, ErrCode)
	if field == nil {
		return ""
	}

	return field.(string)
}

func ErrorHTTPCode(err error) int {
	field := errorsf.Field(err, ErrHTTPCode)
	if field == nil {
		return 0
	}

	return field.(int)
}

func ErrorDetails(err error) []map[string]interface{} {
	field := errorsf.Field(err, ErrDetails)
	if field == nil {
		return []map[string]interface{}{}
	}

	return field.([]map[string]interface{})
}

type SemanticError struct {
	Err    error
	Msg    string
	Fields []interface{}
}

func (e SemanticError) E() error {
	msg := "semantic error"
	if e.Msg != "" {
		msg += ": " + e.Msg
	}

	err := e.Err
	if err == nil {
		err = errors.New(msg)
	} else {
		err = errors.Wrap(err, msg)
	}

	return errorsf.WithFields(
		err,
		append(e.Fields,
			ErrCode, ErrSemanticError,
			ErrHTTPCode, 500,
		)...,
	)
}
