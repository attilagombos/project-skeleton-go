package validationerr

import (
	"github.com/go-playground/validator"
	"github.com/pkg/errors"
	"gitlab.com/attilagombos/project-skeleton-go/app/schema"
	"gitlab.com/attilagombos/project-skeleton-go/errorsf"
)

type ValidationError struct {
	Err       error
	Msg       string
	PathParam string
}

func (e ValidationError) E() error {
	msg := "validation error"
	if e.PathParam != "" {
		msg += ": invalid path parameter: " + e.PathParam
	}
	if e.Msg != "" {
		msg += ": " + e.Msg
	}

	err := e.Err
	var details []map[string]interface{}
	if err == nil {
		err = errors.New(msg)
	} else if errs, ok := err.(validator.ValidationErrors); ok {
		err = errors.New(msg)
		details = make([]map[string]interface{}, 0, len(errs))
		for _, err := range errs {
			details = append(details, map[string]interface{}{
				"field":   err.Field(),
				"error":   schema.ErrValidation,
				"message": "Field " + err.Namespace() + " failed on: " + err.Tag(),
			})
		}
	} else {
		err = errors.Wrap(err, msg)
	}

	return errorsf.WithFields(
		err,
		schema.ErrCode, schema.ErrValidation,
		schema.ErrHTTPCode, 400,
		schema.ErrDetails, details,
	)
}
