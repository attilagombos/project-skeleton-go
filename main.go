package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/attilagombos/project-skeleton-go/app/config"
	"gitlab.com/attilagombos/project-skeleton-go/app/di"
	"gitlab.com/attilagombos/project-skeleton-go/log"
	"gitlab.com/attilagombos/project-skeleton-go/log/zaplog"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func main() {
	setupLogger(os.Getenv("LOG_LEVEL"))

	fmt.Println("Starting server")

	defer func() {
		if err := recover(); err != nil {
			log.Error(context.Background(), "Service panicked", "error", err)
			os.Exit(1)
		}
	}()

	fmt.Println("Reading configuration")

	cfg, err := readConfig()
	if err != nil {
		log.Panic(context.Background(), "Couldn't load config", "error", err)
	}

	fmt.Println("Creating DI container")

	container, err := di.NewContainer(cfg)
	if err != nil {
		log.Panic(context.Background(), "Setup failed", "error", err)
	}
	defer container.Close()

	errorCh := make(chan error)
	container.RestServer.Start(errorCh)
	container.PollService.StartPolling()

	defer func() {
		err = container.RestServer.Stop(5 * time.Second)
		if err != nil {
			err = errors.Wrap(err, "Rest server graceful shutdown failed")
			log.Panic(context.Background(), err.Error(), "error", err)
		}
		log.Info(context.Background(), "Shutdown complete")
	}()

	log.Info(context.Background(), "Rest server started")

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM)
	select {
	case <-sigs:
	case err := <-errorCh:
		err = errors.Wrap(err, "Rest server fatal error")
		log.Panic(context.Background(), err.Error(), "error", err)
	}
}

func setupLogger(logLevel string) {
	err := zap.RegisterEncoder(zaplog.EncoderType, zaplog.NewEncoder([]string{
		log.AppName,
		log.AppVersion,
	}))
	if err != nil {
		panic(fmt.Sprintf("Couldn't create logger, error: %v", err))
	}

	zapConf := zap.NewProductionConfig()
	zapConf.Encoding = zaplog.EncoderType

	if logLevel == "" {
		logLevel = "INFO"
	}

	zapLogLevel := new(zapcore.Level)
	err = zapLogLevel.Set(logLevel)
	if err != nil {
		panic(fmt.Sprintf("Invalid log level: %s", logLevel))
	}
	zapConf.Level = zap.NewAtomicLevelAt(*zapLogLevel)

	zapLog, err := zapConf.Build()
	if err != nil {
		panic(fmt.Sprintf("Couldn't create logger, error: %v", err))
	}
	zapLog = zapLog.With(
		zap.String(log.AppName, config.AppName),
		zap.String(log.AppVersion, config.AppVersion),
	)
	log.SetGlobalLogger(zaplog.NewLogger(zapLog, log.NoopContextMapper{}))
}

func readConfig() (*config.Config, error) {
	cfg := config.DefaultConfig()
	cfg.Port = 43212
	cfg.RedisHost = "localhost"
	cfg.RedisPort = 6379
	cfg.ProjectName = "proemergotech-public/project-skeleton-go"
	cfg.PollPeriod = 60

	if port, ok := os.LookupEnv("PORT"); ok {
		p, err := strconv.Atoi(port)
		if err != nil {
			return nil, errors.Wrapf(err, "invalid value for PORT env: %v", port)
		}
		cfg.Port = p
	}
	if redisHost, ok := os.LookupEnv("REDIS_HOST"); ok {
		cfg.RedisHost = redisHost
	} else {
		return nil, errors.New("missing mandatory env: REDIS_HOST")
	}
	if redisPort, ok := os.LookupEnv("REDIS_PORT"); ok {
		p, err := strconv.Atoi(redisPort)
		if err != nil {
			return nil, errors.Wrapf(err, "invalid value for REDIS_PORT env: %v", redisPort)
		}
		cfg.RedisPort = p
	}
	if redisDatabase, ok := os.LookupEnv("REDIS_DATABASE"); ok {
		db, err := strconv.Atoi(redisDatabase)
		if err != nil {
			return nil, errors.Wrapf(err, "invalid value for REDIS_DATABASE env: %v", redisDatabase)
		}
		cfg.RedisPort = db
	}
	if projectName, ok := os.LookupEnv("GITLAB_PROJECT_NAME"); ok {
		cfg.ProjectName = projectName
	} else {
		return nil, errors.New("missing mandatory env: GITLAB_PROJECT_NAME")
	}
	if pollPeriod, ok := os.LookupEnv("POLL_PERIOD"); ok {
		seconds, err := strconv.Atoi(pollPeriod)
		if err != nil {
			return nil, errors.Wrapf(err, "invalid value for POLL_PERIOD env: %v", pollPeriod)
		}
		cfg.PollPeriod = seconds
	}

	return cfg, nil
}
