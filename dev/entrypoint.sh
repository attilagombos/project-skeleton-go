#!/usr/bin/env bash
set -e

if [[ "$1" = 'run' ]]; then
    chmod +x ./build.sh

    exec CompileDaemon \
    -build="./build.sh dev /tmp/project-skeleton-go" \
    -command="/tmp/project-skeleton-go ${@:2}" \
    -exclude-dir=vendor \
    -graceful-kill=true \
    -log-prefix=false
fi

exec "$@"
